﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Contentandviews.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Page1()
        {
            return Content("<h1>This is an H1 tag</h1>", "text/html");
        }

        public IActionResult Page2()
        {
            return Content("<h1>This is an H1 tag</h1><h2>This is an H2 tag</h2>", "text/html");
        }

        public IActionResult Page3()
        {
            return Content("Hey isn't this just text?", "text/plain", Encoding.UTF8);
        }

        public IActionResult Page4()
        {
            return Content("A", "text/plain", Encoding.ASCII);
        }

        public IActionResult Page5()
        {
            string thisvariable = "This is a variable that I'm going to return";
            return Content(thisvariable);
        }

        public IActionResult Page6()
        {
            string thisothervariable = "This is a different variable that I'm going to return";
            return Content("<h1>" + thisothervariable + "</h1>", "text/html");
        }

        public IActionResult Page7()
        {
            Random rnd = new Random();
            string yetanothervariable = "json specific or content, doesn't seem to matter";

            if (rnd.Next(1, 3) == 1)
            {
                return Content(yetanothervariable, "application/json");
            }
            else
            {
                return Json(yetanothervariable);
            }            
        }

        public IActionResult Page8()
        {
            string[][][] jag =
            {
                new string[][]
                {
                    new string[] { "First Name: James", "Last Name: Beck", "Fav. Food: Spaghetti" }
                },
                new string[][]
                {
                    new string[] { "First Name: Mike", "Last Name: Tindall 'Tindog'/'TinBeast'/'TinMan'", "Fav. Food: Chips" }
                }
            };

            return Json(jag);
        }
    }
}
